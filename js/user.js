function signUp() {
	var name = document.getElementById("name").value;
	var lastname = document.getElementById("lastname").value;
	var username = document.getElementById("username").value;
	var password = document.getElementById("inputPassword").value;
	var email = document.getElementById("inputEmail").value;
	var accept = document.getElementById("accept");
	var div = document.getElementById("divhidden");
	var label = document.getElementById('lbltipAddedComment');

	var params = 'name=' + name + '&lastname=' + lastname + '&username='
			+ username + '&password=' + password + '&email=' + email;
	//
	// if (name == "" || lastname == "" || username == "" || password == ""
	// || email == "") {
	// return;
	// }
	if (accept.checked == true
			&& !(name == "" || lastname == "" || username == ""
					|| password == "" || email == "")) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		}

		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				status = xmlhttp.status;
				// alert(status);
				if (status == 200) {
					console.log(xmlhttp.responseText);
						window.location.href = "./login.html";

				}

			}

		}

		xmlhttp.open("POST", ip + "campi_maana/adduser.php", true);
		xmlhttp.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		xmlhttp.send(params);
	} else if (accept.checked == false) {
		label.innerHTML = "You should accept Terms and Condition";
		div.hidden = false;
	} else if (name == "" || lastname == "" || username == "" || password == ""
			|| email == "") {
		label.innerHTML = "Please fill all the fields";
		div.hidden = false;
	}
}

function signIn() {
	var password = document.getElementById("password").value;
	var email = document.getElementById("email").value;
	var div = document.getElementById("divhidden");
	var label = document.getElementById('lbltipAddedComment');
	var rememberme = document.getElementById('rememberme');
	var params = 'email=' + email + '&password=' + password;

	if (!(email == "" || password == "")) {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				status = xmlhttp.status;
			//	alert(status);
				if (status == 200) {
				//	var res = xmlhttp.response;
					var jsonResponse = JSON.parse(xmlhttp.response);
					var u = jsonResponse.user[0];
					sessionStorage.setItem("id", u.id );
					sessionStorage.setItem("name", u.name);
					sessionStorage.setItem("lastname", u.lastname);
					sessionStorage.setItem("email", u.email);
					sessionStorage.setItem("username", u.username);
					sessionStorage.setItem("photo", u.photo );
					sessionStorage.setItem("remember",rememberme.checked);
					window.location.href = "./index.html";
				}else {
					
					label.innerHTML = "Wrong email/password.";
					div.hidden = false
				}

			}

		}
		xmlhttp.open("POST", ip + "campi_maana/login.php", true);
		xmlhttp.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
	
		xmlhttp.send(params);

	}else{
		label.innerHTML = "Empty fields.";
		div.hidden = false
	}

}
function fill() {
	var img = document.getElementById("imgprofil");
	var name = document.getElementById("name");

	img.src = ip+"campi_maana/upload/"+sessionStorage.getItem("photo");
	var n = sessionStorage.getItem("name");
	var l = sessionStorage.getItem("lastname");
	name.innerHTML= n.concat(" ",l);
//	alert(" test ");
//	alert(sessionStorage.getItem("lastname"));
}
function logout(){
	sessionStorage.clear();
	window.location.href = "./login.html";
}

function redirect(){
	if(sessionStorage.length != 0 && sessionStorage.getItem("remember") == "true"){
		window.location.href = "./index.html";
	}else{
		sessionStorage.clear();
	}
}